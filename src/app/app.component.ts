import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'projet-post';


  posts = [
    {
      title: 'Mon premier post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce interdum massa neque, mattis aliquam tellus maximus vel. Mauris laoreet nunc eget quam tempor semper.',
      loveIts: 3
    },
    {
      title: 'Mon deuxième post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce interdum massa neque, mattis aliquam tellus maximus vel. Mauris laoreet nunc eget quam tempor semper.',
      loveIts: -2
    },
    {
      title: 'Encore un post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce interdum massa neque, mattis aliquam tellus maximus vel. Mauris laoreet nunc eget quam tempor semper.',
      loveIts: 0
    }
  ];

}
