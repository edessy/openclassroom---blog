import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLoveIts : number;
  @Input() postCreatedAt : Date;

  constructor() {
    this.postCreatedAt = new Date('2017-10-24T11:00:00');
  }

  ngOnInit() {
  }

  onLoveIt() {
    this.postLoveIts++;
  }

  onDontLoveIt() {
    this.postLoveIts--;
  }

}
